package com.curso.udemy.springboot.zuul.springbootzuulzervice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@EnableZuulProxy
@SpringBootApplication
public class SpringbootZuulZerviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootZuulZerviceApplication.class, args);
	}

}
