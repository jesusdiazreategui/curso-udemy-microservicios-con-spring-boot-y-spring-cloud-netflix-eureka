package com.curso.udemy.springboot.item.springbootservicioitem.cliente;

import com.curso.udemy.springboot.item.springbootservicioitem.model.Producto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(name = "servicio-producto")
public interface ProductoClienteRest {

    @GetMapping("/listar")
    public List<Producto> listar();

    @GetMapping("/ver/{id}")
    public Producto detalle(@PathVariable Integer id);

    @PostMapping("/crear")
    public Producto crear(@RequestBody Producto producto);

    @PutMapping("/editar/{id}")
    public Producto update(@RequestBody Producto producto, @PathVariable Integer id);

    @DeleteMapping("/eliminar/{id}")
    public void eliminar(@PathVariable Integer id);

}
