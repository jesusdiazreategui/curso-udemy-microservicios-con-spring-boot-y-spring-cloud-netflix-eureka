package com.curso.udemy.springboot.item.springbootservicioitem.service;

import com.curso.udemy.springboot.item.springbootservicioitem.cliente.ProductoClienteRest;
import com.curso.udemy.springboot.item.springbootservicioitem.model.Item;
import com.curso.udemy.springboot.item.springbootservicioitem.model.Producto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service("serviceFeign")
public class ItemServiceFeign implements IItemService{

    @Autowired
    private ProductoClienteRest clienteRest;

    @Override
    public List<Item> findAll() {
        return clienteRest.listar().stream()
                .map(p -> new Item(p, 1))
                .collect(Collectors.toList());
    }

    @Override
    public Item findById(Integer id, Integer cantidad) {
        return new Item(clienteRest.detalle(id), cantidad);
    }

    @Override
    public Producto save(Producto producto) {
        return clienteRest.crear(producto);
    }

    @Override
    public Producto update(Producto producto, Integer id) {
        return clienteRest.update(producto, id);
    }

    @Override
    public void delete(Integer id) {
        clienteRest.eliminar(id);
    }
}
