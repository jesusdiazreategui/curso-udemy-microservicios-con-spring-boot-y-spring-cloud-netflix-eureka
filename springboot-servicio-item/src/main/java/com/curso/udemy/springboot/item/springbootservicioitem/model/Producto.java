package com.curso.udemy.springboot.item.springbootservicioitem.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Producto {

    private Integer id;

    private String nombre;

    private Double precio;

    private Date createAt;

    private Integer port;

}
