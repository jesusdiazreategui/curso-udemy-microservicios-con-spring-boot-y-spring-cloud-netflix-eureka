package com.curso.udemy.springboot.item.springbootservicioitem.cotroller;

import com.curso.udemy.springboot.item.springbootservicioitem.model.Item;
import com.curso.udemy.springboot.item.springbootservicioitem.model.Producto;
import com.curso.udemy.springboot.item.springbootservicioitem.service.IItemService;
import org.bouncycastle.asn1.esf.SPuri;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ItemController {


    @Autowired
    @Qualifier("serviceFeign")
    private IItemService itemService;

//    @Autowired
//    @Qualifier("serviceRestTemplate")
//    private IItemService itemService;

    @GetMapping("/listar")
    public List<Item>  listar(){
        return itemService.findAll();
    }

    @GetMapping("/ver/{id}/cantidad/{cantidad}")
    public Item detalle(@PathVariable Integer id, @PathVariable Integer cantidad) {
        return itemService.findById(id, cantidad);
    }

    @PostMapping("/crear")
    @ResponseStatus(HttpStatus.CREATED)
    public Producto crear(@RequestBody Producto producto) {
        return itemService.save(producto);
    }

    @PutMapping("/editar/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public Producto editar(@RequestBody Producto producto, @PathVariable Integer id) {
        return itemService.update(producto, id);
    }

    @DeleteMapping("/eliminar/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void eliminar(@PathVariable Integer id) {
        itemService.delete(id);
    }

}
