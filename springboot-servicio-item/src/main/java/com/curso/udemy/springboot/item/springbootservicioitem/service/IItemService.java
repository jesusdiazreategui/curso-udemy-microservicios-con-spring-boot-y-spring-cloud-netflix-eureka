package com.curso.udemy.springboot.item.springbootservicioitem.service;

import com.curso.udemy.springboot.item.springbootservicioitem.model.Item;
import com.curso.udemy.springboot.item.springbootservicioitem.model.Producto;
import org.bouncycastle.pqc.crypto.util.PQCOtherInfoGenerator;

import java.util.List;

public interface IItemService {

    public List<Item> findAll();

    public Item findById(Integer id, Integer cantidad);

    public Producto save(Producto producto);

    public Producto update(Producto producto, Integer id);

    public void delete(Integer id);
}
