package com.curso.udemy.springboot.producto.springbootservicioproducto.repository;


import com.curso.udemy.springboot.producto.springbootservicioproducto.models.entity.Producto;
import org.springframework.data.repository.CrudRepository;

public interface ProductoRepository extends CrudRepository<Producto, Integer> {
}
