package com.curso.udemy.springboot.producto.springbootservicioproducto.service;

import com.curso.udemy.springboot.producto.springbootservicioproducto.models.entity.Producto;
import com.curso.udemy.springboot.producto.springbootservicioproducto.repository.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ProductoServiceImpl implements IProductoService{

    @Autowired
    private ProductoRepository productoRepository;

    @Override
    @Transactional(readOnly = true)
    public List<Producto> findAll() {
        return (List<Producto>) productoRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Producto findById(Integer id) {
        return productoRepository.findById(id)
                .orElseThrow(() -> {
                    throw new NullPointerException("No existe el producto");
                }
        );
    }

    @Override
    @Transactional
    public Producto save(Producto producto) {
        return productoRepository.save(producto);
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        productoRepository.deleteById(id);
    }
}
