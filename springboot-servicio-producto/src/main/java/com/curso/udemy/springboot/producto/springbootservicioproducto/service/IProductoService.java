package com.curso.udemy.springboot.producto.springbootservicioproducto.service;

import com.curso.udemy.springboot.producto.springbootservicioproducto.models.entity.Producto;

import java.util.List;

public interface IProductoService {
    public List<Producto> findAll();

    public Producto findById(Integer id);

    public Producto save(Producto producto);

    public void deleteById(Integer id);
}
